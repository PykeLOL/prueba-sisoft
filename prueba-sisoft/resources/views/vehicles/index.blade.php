@extends('layouts.app')
@section('title', 'Vehiculos')

@section('content')
<div class="container">
    <a class="btn btn-primary" href="{{ route('owners.index') }}">Propietarios</a>
    <div class="d-flex justify-content-between align-items-center mb-3">
        <h2 class="mb-0">Vehiculos:</h2>
        <form action="{{ route('vehicles.search') }}" method="GET" class="mb-3">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Placa, Identificación, Marca o Linea" name="searchTerm">
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="submit">Buscar</button>
                </div>
            </div>
        </form>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createVehicleModal">
            Crear
        </button>
    </div>
    <table class="table table-striped">
        <thead class="thead-dark">
            <tr>
                <th>Placa</th>
                <th>Marca</th>
                <th>Linea</th>
                <th>Precio</th>
                <th>Propietarios</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @forelse($vehicles as $vehicle)
                <tr>
                    <td>{{ $vehicle->plate }}</td>
                    <td>{{ $vehicle->brand }}</td>
                    <td>{{ $vehicle->line }}</td>
                    <td>$ {{ number_format($vehicle->price) }}</td>
                    <td>
                        @forelse ($vehicle->owners as $owner)
                            <li>{{ $owner->identification }} {{ $owner->name }} {{ $owner->last_name }}</li>
                        @empty
                            <em>No tiene propietarios</em>
                        @endforelse
                    </td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Acciones">
                            <button type="button" class="btn btn-primary btn-associate" data-toggle="modal" data-target="#associate" data-plate="{{ $vehicle->plate }}" data-owners="{{ route('vehicles.available-owners', ['plate' => $vehicle->plate]) }}">
                                Asignar
                            </button>
                            <button type="button" class="btn btn-success" onclick="openLiquidationModal('{{ $vehicle->plate }}')">Liquidar</button>
                        </div>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="4">No hay vehiculos registrados.</td>
                </tr>
            @endforelse
        </tbody>
    </table>
</div>

<!-- Modal Liquidacion -->
<div class="modal fade" id="liquidarVehiculoModal" tabindex="-1" role="dialog" aria-labelledby="liquidarVehiculoModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="liquidarVehiculoModalLabel">Liquidación de Vehículo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="liquidarVehiculoModalBody">
                <!-- ... -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Asignar Propietarios -->
<div class="modal fade" id="associate" tabindex="-1" role="dialog" aria-labelledby="associateLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="associateLabel">Asignar Propietarios</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="associateForm">
                    @csrf
                    <input type="hidden" id="associatePlate" name="plate" value="">
                    <div class="form-group">
                        <label for="owners">Selecciona propietarios:</label>
                        <select multiple class="form-control" id="owners" name="owners[]" required>
                            <!-- Propietarios -->
                        </select>
                    </div>
                    <button type="button" class="btn btn-primary" id="btnSubmitAssociate">
                        Asignar
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Crear Vehiculo -->
<div class="modal fade" id="createVehicleModal" tabindex="-1" role="dialog" aria-labelledby="createVehicleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createVehicleModalLabel">Crear Vehículo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="createVehicleForm">
                    @csrf
                    <div class="form-group">
                        <label for="plate">Placa</label>
                        <input type="text" class="form-control" id="plate" name="plate" required>
                    </div>
                    <div class="form-group">
                        <label for="brand">Marca</label>
                        <input type="text" class="form-control" id="brand" name="brand" required>
                    </div>
                    <div class="form-group">
                        <label for="line">Linea</label>
                        <input type="text" class="form-control" id="line" name="line" required>
                    </div>
                    <div class="form-group">
                        <label for="price">Precio</label>
                        <input type="number" class="form-control" id="price" name="price" required>
                    </div>
                    <button type="button" class="btn btn-primary" onclick="submitForm()">Guardar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var associateModal = new bootstrap.Modal(document.getElementById('associate'));

        $('.btn-associate').on('click', function (event) {
            var button = event.currentTarget;
            var plate = button.getAttribute('data-plate');
            var ownersUrl = button.getAttribute('data-owners');

            document.getElementById('owners').innerHTML = '';

            fetch(ownersUrl)
                .then(response => response.json())
                .then(data => {
                    var owners = data.owners;

                    owners.forEach(function (owner) {
                        var option = document.createElement('option');
                        option.value = owner.identification;
                        option.text = owner.name + ' ' + owner.last_name;
                        document.getElementById('owners').appendChild(option);
                    });

                    document.getElementById('associatePlate').value = plate;

                    associateModal.show();
                })
                .catch(error => {
                    console.error('Error fetching owners:', error);
                });
        });
    });
</script>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var associateForm = document.getElementById('associateForm');

        function submitAssociateForm() {
            var formData = new FormData(associateForm);

            fetch('{{ route('vehicles.associate') }}', {
                method: 'POST',
                body: formData
            })
            .then(response => response.json())
            .then(data => {
                console.log(data.message);
                var associateModal = new bootstrap.Modal(document.getElementById('associate'));
                associateModal.hide();
                location.reload();
            })
            .catch(error => {
                console.error('Error al asignar propietarios:', error);
            });
        }

        document.getElementById('btnSubmitAssociate').addEventListener('click', function () {
            submitAssociateForm();
        });
    });
</script>
<script>
    function submitForm() {
    var formData = {
        plate: $('#plate').val(),
        brand: $('#brand').val(),
        line: $('#line').val(),
        price: $('#price').val()
    };

    fetch('/api/vehicles/create', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    })
    .then(response => response.json())
    .then(data => {
        if (data.errors) {
            $('.error-message').remove();

            Object.entries(data.errors).forEach(([field, error]) => {
                $(`#${field}`).after(`<div class="error-message text-danger">${error[0]}</div>`);
            });
        } else {
            console.log(data.message);
            $('#createVehicleModal').modal('hide');
            location.reload();
        }
    })
    .catch(error => {
        console.error('Hubo un problema con la operación de fetch:', error);
    });
}
</script>

<script>
    function openLiquidationModal(plate) {
        fetch(`/api/vehicles/liquidation/${plate}`)
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(data => {
                const ownersInfo = data.vehicle.owners.map(owner => `${owner.name} ${owner.last_name}`).join(', ');

                $('#liquidarVehiculoModalBody').html(`
                    <strong>Placa:</strong> ${data.vehicle.plate}<br>
                    <strong>Marca:</strong> ${data.vehicle.brand}<br>
                    <strong>Línea:</strong> ${data.vehicle.line}<br>
                    <strong>Propietarios:</strong> ${ownersInfo}<br>
                    <strong>Total:</strong> $${data.total.toFixed(2)}
                `);

                $('#liquidarVehiculoModal').modal('show');
            })
            .catch(error => {
                console.error('There was a problem with the fetch operation:', error);
            });
    }
</script>
@endsection
