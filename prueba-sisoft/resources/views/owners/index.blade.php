@extends('layouts.app')
@section('title', 'Propietarios')

@section('content')
<div class="container">
    <a class="btn btn-primary" href="{{ route('vehicles.index') }}">Vehiculos</a>
    <div class="d-flex justify-content-between align-items-center mb-3">
        <h2 class="mb-0">Propietarios:</h2>
        <form action="{{ route('owners.search') }}" method="GET" class="mb-3">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Identificación, Placa, Nombre, Apellido" name="searchTerm">
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="submit">Buscar</button>
                </div>
            </div>
        </form>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createOwnerModal">
            Crear
        </button>
    </div>
    <table class="table table-striped">
        <thead class="thead-dark">
            <tr>
                <th>Identificación</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Vehiculos</th>
            </tr>
        </thead>
        <tbody>
            @forelse($owners as $owner)
                <tr>
                    <td>{{ $owner->identification }}</td>
                    <td>{{ $owner->name }}</td>
                    <td>{{ $owner->last_name }}</td>
                    <td>
                        @forelse ($owner->vehicles as $vehicle)
                            <li>{{ $vehicle->plate }} {{ $vehicle->brand }} {{ $vehicle->line }}</li>
                        @empty
                            No tiene vehiculos
                        @endforelse
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="4">No hay propietarios registrados.</td>
                </tr>
            @endforelse
        </tbody>
    </table>
</div>

<!-- Modal Crear Propietario -->
<div class="modal fade" id="createOwnerModal" tabindex="-1" role="dialog" aria-labelledby="createOwnerModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createOwnerModalLabel">Crear Propietario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="createOwnerForm">
                    @csrf
                    <div class="form-group">
                        <label for="identification">Identificación</label>
                        <input type="text" class="form-control" id="identification" name="identification" required>
                    </div>
                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input type="text" class="form-control" id="name" name="name" required>
                    </div>
                    <div class="form-group">
                        <label for="last_name">Apellido</label>
                        <input type="text" class="form-control" id="last_name" name="last_name" required>
                    </div>
                    <button type="button" class="btn btn-primary" onclick="submitOwnerForm()">Guardar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script>
    function submitOwnerForm() {
    var formData = {
        name: $('#name').val(),
        last_name: $('#last_name').val(),
        identification: $('#identification').val(),
    };

    fetch('/api/owners/create', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    })
    .then(response => response.json())
    .then(data => {
        if (data.errors) {
            $('.error-message').remove();

            Object.entries(data.errors).forEach(([field, error]) => {
                $(`#${field}`).after(`<div class="error-message text-danger">${error[0]}</div>`);
            });
        } else {
            console.log(data.message);
            $('#createOwnerModal').modal('hide');
            location.reload();
        }
    })
    .catch(error => {
        console.error('Hubo un problema con la operación de fetch:', error);
    });
}

</script>

@endsection
