<?php

namespace App\Http\Controllers;

use App\Models\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class VehicleController extends Controller
{
    public function index()
    {
        $vehicles = Vehicle::all();

        return view('vehicles.index', compact('vehicles'));
    }

    public function search(Request $request)
    {
        $searchTerm = $request->input('searchTerm');

        $vehicles = Vehicle::where('plate', 'like', "%$searchTerm%")
            ->orWhere('brand', 'LIKE', "%{$searchTerm}%")
            ->orWhere('line', 'LIKE', "%{$searchTerm}%")
            ->orWhereHas('owners', function ($query) use ($searchTerm) {
                $query->where('identification', 'like', "%$searchTerm%");
            })
            ->get();

        return view('vehicles.index', compact('vehicles'));
    }

    public function create(Request $request)
    {
        Log::info($request);
        $validator = Validator::make($request->all(), [
            'plate' => 'required|string|max:255|unique:vehicles',
            'brand' => 'required|string|',
            'line' => 'required|string|',
            'price' => 'required|numeric|min:1',
        ], [
            'plate.required' => 'El campo Placa es requerido.',
            'plate.unique' => 'La Placa ya está registrada.',
            'brand.required' => 'El campo Marca es requerido.',
            'line.required' => 'El campo Linea es requerido.',
            'price.required' => 'El campo Precio es requerido.',
            'price.numeric' => 'El campo Precio debe ser un número.',
            'price.min' => 'El campo Precio debe ser mayor a 0.',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => 'Error de validación', 'errors' => $validator->errors()], 422);
        }

        $vehicle = Vehicle::create([
            'plate' => $request->get('plate'),
            'brand' => $request->get('brand'),
            'line' => $request->get('line'),
            'price' => $request->get('price'),
        ]);

        return response()->json(compact('vehicle'),201);
    }

    public function liquidation($plate)
    {
        try {
            $vehicle = Vehicle::with('owners')->where('plate', $plate)->firstOrFail();
            $total = $vehicle->price * 1.5;

            return response()->json(['vehicle' => $vehicle, 'total' => $total]);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'Vehiculo no encontrado'], 404);
        }
    }
}
