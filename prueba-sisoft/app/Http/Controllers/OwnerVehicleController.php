<?php

namespace App\Http\Controllers;

use App\Models\Owner;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class OwnerVehicleController extends Controller
{
    public function associate(Request $request)
    {
        Log::info($request);
        $validator = Validator::make($request->all(), [
            'plate' => 'required|string|exists:vehicles',
            'owners.*' => 'required|string|exists:owners,identification',
        ]);

        if($validator->fails()){
                return response()->json($validator->errors()->toJson(),400);
        }

        $vehicle = Vehicle::where('plate', $request->input('plate'))->first();
        foreach ($request->input('owners') as $ownerId) {
            $owner = Owner::where('identification', $ownerId)->first();
            $vehicle->owners()->attach($owner);
        }

        return response()->json(['message' => 'Asociación exitosa']);
    }

    public function availableOwners($plate)
    {
        try {
            $vehicle = Vehicle::where('plate', $plate)->firstOrFail();
            $allOwners = Owner::all();
            $availableOwners = $allOwners->diff($vehicle->owners);

            return response()->json(['owners' => $availableOwners]);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'Vehículo no encontrado'], 404);
        }
    }

    public function consult()
    {
        $ownersVehiclesCount = Owner::withCount('vehicles')->get();

        return response()->json(['owners_vehicles_count' => $ownersVehiclesCount], 200);
    }
}
