<?php

namespace App\Http\Controllers;

use App\Models\Owner;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class OwnerController extends Controller
{
    public function index()
    {
        $owners = Owner::all();

        return view('owners.index', compact('owners'));
    }

    public function search(Request $request)
    {
        $searchTerm = $request->input('searchTerm');

        $owners = Owner::when($searchTerm, function ($query) use ($searchTerm) {
            $query->where('name', 'LIKE', "%{$searchTerm}%")
                ->orWhere('last_name', 'LIKE', "%{$searchTerm}%")
                ->orWhere('identification', 'LIKE', "%{$searchTerm}%")
                ->orWhereHas('vehicles', function ($query) use ($searchTerm) {
                    $query->where('plate', 'LIKE', "%{$searchTerm}%");
                });
        })->get();

        return view('owners.index', ['owners' => $owners]);
    }

    public function create(Request $request)
    {
        Log::info($request);
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'identification' => 'required|numeric|unique:owners',
        ], [
            'name.required' => 'El campo Nombre es requerido.',
            'last_name.required' => 'El campo Apellido es requerido.',
            'identification.required' => 'El campo Identificación es requerido.',
            'identification.numeric' => 'El campo Identificación debe ser un número.',
            'identification.unique' => 'La identificación ya está registrada.',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => 'Error de validación', 'errors' => $validator->errors()], 422);
        }

        $owner = Owner::create([
            'name' => $request->get('name'),
            'last_name' => $request->get('last_name'),
            'identification' => $request->get('identification'),
        ]);
        return response()->json(compact('owner'),201);
    }
}
