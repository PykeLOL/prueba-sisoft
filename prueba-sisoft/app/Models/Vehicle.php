<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    use HasFactory;

    protected $primaryKey = 'vehicle_id';
    protected $fillable = ['plate', 'brand', 'line', 'price'];

    public function owners()
    {
        return $this->belongsToMany(Owner::class, 'owner_vehicle', 'vehicle_id', 'owner_id');
    }
}
