<?php

namespace App\Models;

use App\Models\Vehicle;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Owner extends Model
{
    use HasFactory;

    protected $primaryKey = 'owner_id';
    protected $fillable = ['identification', 'name', 'last_name'];

    public function vehicles()
    {
        return $this->belongsToMany(Vehicle::class, 'owner_vehicle', 'owner_id', 'vehicle_id');
    }
}
