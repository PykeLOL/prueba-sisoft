<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OwnerController;
use App\Http\Controllers\VehicleController;

Route::get('/', function () {
    return redirect()->route('vehicles.index');
});

Route::get('/owners', [OwnerController::class, 'index'])->name('owners.index');
Route::get('/owners/search', [OwnerController::class, 'search'])->name('owners.search');

Route::get('/vehicles', [VehicleController::class, 'index'])->name('vehicles.index');
Route::get('/vehicles/search', [VehicleController::class, 'search'])->name('vehicles.search');


