<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OwnerController;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\OwnerVehicleController;

Route::prefix('vehicles')->group(function () {
    Route::post('/create', [VehicleController::class, 'create']);
    Route::get('/liquidation/{plate}', [VehicleController::class, 'liquidation']);
    Route::post('/associate', [OwnerVehicleController::class, 'associate'])->name('vehicles.associate');
    Route::get('/available-owners/{plate}', [OwnerVehicleController::class, 'availableOwners'])->name('vehicles.available-owners');
});

Route::prefix('owners')->group(function () {
    Route::post('/create', [OwnerController::class, 'create']);
});
Route::get('/owners-vehicles', [OwnerVehicleController::class, 'consult']);


